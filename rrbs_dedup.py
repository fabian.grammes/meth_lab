# Fabian
import sys
import contextlib
import argparse
import HTSeq
import collections

# create pareser object
parser = argparse.ArgumentParser()

# add command line options
parser.add_argument('-i', '--idx', type=str,
                    help='File containing the indices in .fastq format')
parser.add_argument('-a', '--algn', type=str,
                    help='Alignment file produced by bismark (.sam/.bam)')
parser.add_argument('--append_idx', action="store_true",
                    help='''Appends the index sequence to the end of the read name''')
parser.add_argument('-l', '--log', action="store_true",
                    help = 'Write a log file')

# read the command line inputs
args = parser.parse_args()

args = parser.parse_args(['-i', 'test/bismark_test.index.fq.gz',
                          '-a', 'test/bismark_test.bam',
                          '-l'])

## -----------------------------------------------------------------------------

class RrbsIdx:
    '''
    Class to read in the index file and to make the index 
    availble by read name
    '''
    def __init__(self, fq_file):
        self.data = dict()
        for f in HTSeq.FastqReader( fq_file, qual_scale="phred"):
            f.name = f.name.replace(" ", "_" )
            self.data[f.name] = f.seq[6:]
            
    def fetch(self, read_name):
        out = self.data[ read_name ]
        return( out )

class GroupReads:
    '''
    Class to identify the best read (highest mapping quality) from a set of
    reads mapping to the same genomic position
    '''
    def __init__(self, data):
        self.data = list(data)
        self.ids = [i[0] for i in self.data]
        self.qual = [i[1] for i in self.data]
        bi = [i for i,j in enumerate(self.qual) if j == max(self.qual)]
        bi = self.ids[ bi[0] ]
        nbi = [i for i in self.ids if i != bi]
        self.best = bi
        self.nonbest = nbi

    def b(self):
        return( self.best )

    def nonb(self):
        return( self.nonbest )

def algn_reader(filename):
    if filename.endswith(".bam"):
        algn = HTSeq.BAM_Reader(args.algn)
    elif filename.endswith(".sam"):
        algn = HTSeq.SAM_Reader(args.algn)
    return algn
            
## -----------------------------------------------------------------------------

## Step1: Read in .fq index file in     
idx = RrbsIdx( args.idx )

## TESTING
#idx.fetch('M02210:39:000000000-ACGGD:1:1101:17841:2058_1:N:0:1')

## Step2: Read all pe-reads into a set defied by start and end position
##        of the pair.
algn = algn_reader( args.algn )
genomic_loc = HTSeq.GenomicArrayOfSets( "auto", stranded=False )
for f, s in HTSeq.pair_SAM_alignments( algn ):
    pos = [f.iv.start, s.iv.start, f.iv.end, s.iv.end]
    piv = HTSeq.GenomicInterval( f.iv.chrom, min(pos), max(pos),'.')
    try:
        idx_seq = idx.fetch( f.read.name )
    except KeyError:
        print("ERROR read %")
    genomic_loc[ piv ] += (f.read.name, piv.start, piv.end, f.aQual, idx_seq )

## Step3: Assamble dict for read positions seperated by index
glp = dict()
for iv, stack in genomic_loc.steps():
    ## if the stack is 0 move on
    if len(stack) == 0:
        continue
    for s in stack:
        read = list(s)
        locus = "%s:%d:%d" % (iv.chrom, iv.start, iv.end)
        if locus not in glp:
            glp[locus] = dict()
            adater = read[4]    
        if adater not in glp[locus]:
            glp[locus][adater] = set()
        glp[locus][adater].add( (read[0], read[3]) )
            
        
## Step4: Loop through the glp dict, identify the best read and write
##        the rest to `bad_reads`
bad_reads = set()
for i in glp:
    for ii in glp[i]:
        gr = GroupReads(glp[i][ii])
        for j in gr.nonb():
            bad_reads.add( j )

            
## Step5: Final loop through the file again and skip pe-reads where the
##        read name can be found within the `bad_reads` set.
pe_counts = {'kept':0, 'removed':0,'total':0}
for f, s in HTSeq.pair_SAM_alignments( algn ):
    pe_counts['total']+=1
    if f.read.name in bad_reads:
        pe_counts['removed']+=1
        continue
    if args.append_idx:
        f.read.name = f.read.name+"_"+idx.fetch( f.read.name )
        s.read.name = s.read.name+"_"+idx.fetch( s.read.name )
    sys.stdout.write("%s\n" % f.get_sam_line())
    sys.stdout.write("%s\n" % s.get_sam_line())
    pe_counts['kept']+=1


if args.log :
    with open( args.algn.replace(".bam",".dedup.log"),"w") as log_file:
        log_file.write('Removing PCR duplicates from aligned PE-reads\n')
        log_file.write('total\tkept\tremoved\n')
        log_file.write('%d\t%d\t%d\n' %
                       (pe_counts['total'], pe_counts['kept'], pe_counts['removed']))
        table = {}
        for i in glp:
            for ii in glp[i]:
                lii = len(glp[i][ii])
                if lii not in table:
                    table[lii] = 0
                table[lii]+=1

        table = collections.OrderedDict(table)
        log_file.write('\n')
        log_file.write('PCR duplicates - Table:\n')
        log_file.write('-----------------------------\n')
        log_file.write('loc_occurence\tcount\n')
        for t in table:
            log_file.write('%d\t%d\n' % (t, table[t]))
        

