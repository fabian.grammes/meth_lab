import sys, itertools, argparse, HTSeq, gzip

'''
Useage: python fq_filter.py -a <sam/bam> -f <index.fastq(.gz)> -o
'''

# create pareser object
parser = argparse.ArgumentParser()

# add command line options
parser.add_argument('-f', '--fastq', type=str, help='File containing the indices in .fastq format')
parser.add_argument('-a', '--algn', type=str, help='Bam or Sam file to match')
parser.add_argument('-o', '--out', type=str, help='Output file in .fastq format')

# read the command line inputs
args = parser.parse_args()

#args.fastq = "test_index.fq.gz"
#args.algn = "test.sam"
#args.out = "test_out.fq"

# Read all read names from the sam/bam file into a set (r_set)
r_set = set()
if args.algn.endswith(".bam"):
    algn = HTSeq.BAM_Reader(args.algn)
elif args.algn.endswith(".sam"):
    algn = HTSeq.SAM_Reader(args.algn)
else:
    print "ERROR: File %s has to be in .sam/bam format"
    sys.exit()
for read in algn:
    r_set.add( read.read.name )

# If the .fq index read can be found in the set print
if args.out.endswith(".gz"):
    file_handle = gzip.open(args.out, "wb")
else:
    file_handle = open(args.out, "w")    
with file_handle as out_handle:
    log = {'match':0, 'removed':0 }
    for f in HTSeq.FastqReader( args.fastq , qual_scale="phred"):
        f.name = f.name.replace(" ", "_" )
        if not f.name in r_set:
            log['removed'] += 1
            continue
        else:
            log['match'] += 1
            out_handle.write("@%s\n%s\n+\n%s\n" % (f.name, f.seq, f.qualstr))

sys.stdout.write( "Align file = %d\nFastq file = Kept %d, removed %d\n" % ( len(r_set), log['match'], log['removed'] ))


